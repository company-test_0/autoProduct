const fs = require("fs");

let config = JSON.parse(fs.readFileSync("./config.json",{
    encoding: 'utf-8'
}));

//console.log(config);

// fs.readFile("./index.html",{
//     encoding: 'utf-8'
// },function(err,data){
//     console.log(data);
// });

let regexpFrom = fs.readFileSync(config.regexpFrom,{
    encoding: 'utf-8'
});

let regexpTo = fs.readFileSync(config.regexpTo,{
    encoding: 'utf-8'
});

//console.log(regexpFrom,regexpTo);

let data = fs.readFileSync(config.replaceFile,{
    encoding: 'utf-8'
});

data = data.replace(new RegExp(regexpFrom,'g'), regexpTo);

fs.writeFileSync(config.outputFile,data,{
    encoding: 'utf-8'
});

console.log(data);

